const concat = require("concat");
(async function build() {
  const files = [
    "./dist/angular-micro-fe-elements-two/runtime.js",
    "./dist/angular-micro-fe-elements-two/polyfills.js",
    "./dist/angular-micro-fe-elements-two/main.js",
  ];
  await concat(files, "./dist/angular-micro-fe-elements-two/micro-fe-two.js");
})();
