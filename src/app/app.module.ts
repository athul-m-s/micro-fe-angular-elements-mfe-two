import { Injector, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { createCustomElement } from '@angular/elements';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule],
  providers: [],
  // Keeping bootstrap array to development, while build no need, if user made any changes they need to
  // View the changes in real time so he cant do build all the time, so work with bootstrap array and once
  // changes are done comment it and do the build process

  // bootstrap: [AppComponent],

  //No need of enrty components bcz angular 16 will consider all components as entry components

  // entryComponents:[]
})
export class AppModule {
  constructor(private injector: Injector) {}

  ngDoBootstrap() {
    //if AppComponent is used as webcomponent then must added in entry component but in angular 16 no need.
    const element = createCustomElement(AppComponent, {
      injector: this.injector,
    });

    //'app-micro-fe' will be the tag which can use to show this app in a html file
    customElements.define('app-micro-fe-two', element);
  }
}
